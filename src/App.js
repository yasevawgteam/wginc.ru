import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Header from './js/Header';
import Footer from './js/Footer';
import Body from './js/Body';
import './css/main.css';
import './css/menu.css';
import './css/preloader.css';
import './css/font-awesome.min.css';

class App extends Component {
  constructor(props){
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
    this.scrollBodyToTop = this.scrollBodyToTop.bind(this)
    this.state = {
      goToTopButton: false,
      offset: 0
    }
  }

  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(){
    let pageOffset = window.pageYOffset;

    pageOffset > 500 ? 
      this.setState({
        goToTopButton: true,
        offset: pageOffset
      }) 
      : 
      this.setState({
        goToTopButton: false,
        offset: 0
      })
  }

  scrollBodyToTop(){
    let pageOffset = this.state.offset;
    let scroller = setInterval(() => {

      if(pageOffset > 0) {
        pageOffset -= 30;
        document.body.scrollTop = document.documentElement.scrollTop = pageOffset;
      } else {
        this.setState({offset: 0})
        clearInterval(scroller);
      }
    }, 1);
  }

  render() {
    return (
      <Router>
        <div className="wrap-body" >
          <Header />  

          <Body />     

          <Footer />

          <div onClick={this.scrollBodyToTop} className={this.state.goToTopButton ? "go-top active-go-top" : "go-top"} >
            <i className="fa fa-chevron-up"></i>
          </div>

        </div>
      </Router>
    );
  }
}

export default App;
