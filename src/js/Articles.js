import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from './Loader';

class Articles extends Component {
    constructor(props){
        super(props)
        this.state = {
            link: "https://wginc.ru/rest.php",
            loader: true,
            articles: []
        }
    }

    componentDidMount(){
        fetch(this.state.link)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loader: false,
                    articles: data
                })
            })
    }

    render(){
        if(this.state.loader) return <Loader />
        
        return(
            this.state.articles.map((elem,index) => {
                return(
                    <div key={index} className="wggrid">
                        <article>
                            <div className="row wrap-post">
                                <div className="entry-header">
                                    <span className="time">{elem.date}</span>
                                    <Link to={`${this.props.match.url}/articles/${elem.alias}`}><h2 className="entry-title">{elem.name}</h2></Link>
                                    <span className="cat-links">{elem.author}, SUBJECT</span>
                                </div>
                                <Link to={`${this.props.match.url}/articles/${elem.alias}`}>
                                    <div className="post-thumbnail-wrap col-1-3 zoom-effect">
                                        <img src={`/images/${elem.image}`} alt="articles logo"/>
                                    </div>
                                </Link>
                                <div className="entry-content col-2-3 article-desc">{elem.description}</div>
                            </div>
                        </article>
                    </div>
                )
            })           
        )
    }
}

export default Articles;