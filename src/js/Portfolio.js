import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Projects from './Projects';
import Project from './Project';

class Blog extends Component {
    render(){
        return(
            <Switch>
                <Route path={`${this.props.match.path}/:id`} component={Project} />
                <Route path={this.props.match.path} component={Projects}/>
            </Switch>
        )
    }
}

export default Blog;