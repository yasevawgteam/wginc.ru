import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from './Loader';

class Portfolio extends Component {
    constructor(props){
        super(props)
        this.state = {
            link: "https://wginc.ru/projects.php",
            loader: true,
            projects: []
        }
    }

    componentDidMount(){
        fetch(this.state.link)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loader: false,
                    projects: data
                })
            })
            .catch(e => {
                console.error(e)
            })
    }

    render(){
        if(this.state.loader) return <Loader/>

        return(
            <div id="main-content">
                <div className="wrap-content">
                    <div className="wggrid">
                        <div className="row">
                            {
                                this.state.projects.map((project, index) => {
                                    return(
                                        <div className="col-1-4" key={index}>
                                            <div className="wrap-col projects">
                                                <Link className="portfolio-box zoom-effect fixed" to={`${this.props.match.url}/${project.alias}`}>
                                                    <img src={`/images/portfolio/${project.image}`} className="img-responsive" alt="" />
                                                    <div className="portfolio-box-caption">
                                                        <div className="portfolio-box-caption-content">
                                                        <div className="project-name">
                                                            <span>#{project.name}</span>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                        </div>                                       
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Portfolio;