import React, { Component } from 'react';
import Loader from './Loader';

class Article extends Component {
    constructor(props){
        super(props)
        this.state = {
            loader: true,
            articles: []
        }
    }

    componentDidMount(){
        fetch(`https://wginc.ru/rest.php?id=${this.props.match.params.id}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loader: false,
                    article: data
                })
            })
            .catch(e => {
                console.error(e)
            })
    }

    render(){
        if(this.state.loader) return <Loader />
        
        if(this.state.article == null){
            return(
                <article className="single-post wggrid">
                    <div className="row wrap-post">
                        <div className="entry-header">
                            <h2 className="entry-title">
                                Статья не найдена...
                            </h2>
                        </div>
                    </div>
                </article>
            )
        }

        return(
            <article className="single-post wggrid">
                <div className="row wrap-post">
                    <div className="entry-header">
                        <span className="time">
                            {this.state.article.date}
                        </span>
                        <h1 className="entry-title">
                            {this.state.article.name}
                        </h1>
                        <span className="cat-links">
                            {this.state.article.author}, SUBJECT
                        </span>
                    </div>
                    <div className="post-thumbnail-wrap">
                        <img src={`/images/${this.state.article.image}`} alt="article logo" />
                    </div>
                    <div className="entry-content" dangerouslySetInnerHTML={{__html:this.state.article.text}}></div>
                </div>
            </article>    
        )
    }
}

export default Article;