import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from './Loader';

class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            link: "https://wginc.ru/projects.php?home=true",
            loader: true,
            projects: []
        }
    }

    componentDidMount(){
        fetch(this.state.link)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loader: false,
                    projects: data
                })
            })
            .catch(e => {
                console.error(e)
            })
    }

    getContent(){
        if(this.state.loader){
            return <Loader />
        } else {
            let arColumnProjects = [];

            let arAllProjects = this.state.projects.map((project, index) => {
                return(
                    <Link key={index} className="portfolio-box zoom-effect" to={`/portfolio/${project.alias}`}>
                        <img src={`/images/portfolio/${project.image}`} className="img-responsive" alt="" />
                        <div className="portfolio-box-caption">
                            <div className="portfolio-box-caption-content">
                                <div className="project-name">
                                    <span>#{project.name}</span>
                                </div>
                            </div>
                        </div>
                    </Link>                                     
                )
            })
            
            arColumnProjects.push(
                <div key={"all"}>
                    <div className="col-1-3">
                        <div className="wrap-col">
                            {arAllProjects[0]}
                            {arAllProjects[1]}
                        </div>
                    </div>
                    <div className="col-1-3">
                        <div className="wrap-col">
                            {arAllProjects[2]}
                            {arAllProjects[3]}
                        </div>
                    </div>
                    <div className="col-1-3">
                        <div className="wrap-col">
                            {arAllProjects[4]}
                            {arAllProjects[5]}
                        </div>
                    </div>
                </div>
            )
            return arColumnProjects;
        }
    }

    render(){
        return(
            <div id="main-content">
                <div className="wrap-content">
                    <div className="wggrid">
                        <div className="row">
                            <div className="home-title">
                                <h1 className="entry-title">Последние работы</h1>
                            </div>
                            {
                                this.getContent()
                            }
                        </div>
                    </div>
                </div>
            </div>          
        )
    }
}

export default Home;