import React, { Component } from 'react';
import Loader from './Loader';

class Contacts extends Component {
    constructor(props){
        super(props)
        this.state = {
            name: "",
            email: "",
            message: "",
            confirmed: false,
            validEmail: true,
            validName: true,
            validMessage: true,

            successSend: false,
            successMessage: "Сообщение успешно отправлено",

            errorSend: false,
            errorMessage: "При отправке сообщение произошла ошибка",

            loader: false
        }
    }

    isValidName(){
        this.state.name.length > 0 ? this.setState({validName: true}) : this.setState({validName: false});
        return this.state.name.length > 0 ? true : false;
    }

    isValidEmail(){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.setState({validEmail: re.test(String(this.state.email).toLowerCase())});
        return re.test(String(this.state.email).toLowerCase());
    }

    isValidMessage(){
        this.state.message.length > 10 ? this.setState({validMessage: true}) : this.setState({validMessage: false});
        return this.state.message.length > 10 ? true : false;
    }

    inputHandler(e) {
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        this.setState({
            [e.target.name]: value
        });       
    }

    Button(){
        if(this.state.loader){
            return <Loader />
        } else {
            return(
                <center>
                    <input 
                        onClick={this.sendEmail.bind(this)} 
                        className={this.state.loader ? "sendButton" : "sendButton active_button"} 
                        type="submit" 
                        name="Submit" 
                        value="Отправить" />
                </center>                
            )
        }
    }

    sendEmail(e) {
        e.preventDefault();
        if( this.isValidName() && this.isValidEmail() && this.isValidMessage() && this.state.confirmed ){

            this.setState({loader: true})

            fetch('https://wginc.ru/sendmessage.php', {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: JSON.stringify({
                    name: this.state.name,
                    email: this.state.email,
                    message: this.state.message
                })
            })
            .then(res => res.json())
            .then(res => {
                console.log(res)

                this.setState({
                    successSend: true,
                    errorSend: false,

                    name: "",
                    email: "",
                    message: "",
                    confirmed: false,
                    loader: false
                })                    
            })
            .catch(err => {
                console.error(err)
                this.setState({
                    errorSend: true,
                    loader: false,
                    successSend: false
                })
            });           
        } else {
            this.setState({
                successSend: false
            })
        }
    }

    render(){
        return(
            <section className="content-box wggrid">

                <div id="main-content">
                    <div className="wrap-content">
                        <div className="wggrid">
                            <div className="row">
                                <div className="home-title">
                                    <h1 className="entry-title">Контакты</h1>
                                </div>

                                <div className="col-1-3">
                                    <div className="wrap-col">
                                        <div className="portfolio-box-caption">
                                            <div className="portfolio-box-caption-content">
                                                <div className="project-name">
                                                    <p><i className="fa fa-location-arrow"></i> Москва, Россия</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-1-3">
                                    <div className="wrap-col">
                                        <div className="portfolio-box-caption">
                                            <div className="portfolio-box-caption-content">
                                                <div className="project-name">
                                                    <p><a href="mailto:yasevawgteam@gmail.com"><i className="fa fa-envelope"></i> yasevawgteam@gmail.com</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-1-3">
                                    <div className="wrap-col">
                                        <div className="portfolio-box-caption">
                                            <div className="portfolio-box-caption-content">
                                                <div className="project-name">
                                                    <p><a href="skype:yasevawgteam@gmail.com"><i className="fa fa-skype"></i> yasevawgteam@gmail.com</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div className="row wrap-box">
                    <div className="contact-form">
                        <h3 className="t-center">Обратная связь</h3>
                        <div id="contact_form">
                            <form name="form" id="ff" >
                                <label className="row">
                                    <div className="col-3-6">
                                        <div className="wrap-col">
                                            <input 
                                                onChange={this.inputHandler.bind(this)}
                                                value={this.state.name}
                                                className={this.state.validName ? "" : "error"}
                                                type="text" 
                                                name="name" 
                                                id="name" 
                                                placeholder="Введите имя" 
                                                required="required" />
                                        </div>
                                    </div>
                                    <div className="col-3-6">
                                        <div className="wrap-col">
                                            <input 
                                                onChange={this.inputHandler.bind(this)}
                                                value={this.state.email} 
                                                className={this.state.validEmail ? "" : "error"}
                                                type="email" 
                                                name="email" 
                                                id="email"
                                                placeholder="Введите email" 
                                                required="required" />
                                        </div>
                                    </div>
                                </label>
                                <label className="row">
                                    <div className="wrap-col">
                                        <textarea 
                                            onChange={this.inputHandler.bind(this)}
                                            value={this.state.message} 
                                            name="message" 
                                            id="message" 
                                            className={this.state.validMessage ? "form-control" : "form-control error"} 
                                            rows="4" 
                                            cols="25" 
                                            required="required" 
                                            placeholder="Сообщение" >
                                        </textarea>
                                    </div>
                                </label>
                                <label className={this.state.confirmed ? "row" : "row error_confirm"}>
                                    <input
                                        style={{width: '50px', float: 'left', marginLeft: '20px'}}
                                        name="confirmed"
                                        type="checkbox"
                                        checked={this.state.confirmed}
                                        onChange={this.inputHandler.bind(this)} />
                                        Согласие на обработку персональных данных
                                </label>
                                <label className="row">
                                    <div className="wrap-col">
                                        <div className={this.state.successSend ? "success-send active_send_message" : "success-send"}>{this.state.successMessage}</div>
                                        <div className={this.state.errorSend ? "error-send active_send_message" : "error-send"}>{this.state.errorMessage}</div>
                                    </div>
                                </label>

                                {this.Button()}
                                
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Contacts;