import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Articles from './Articles';
import Article from './Article';

class Blog extends Component {
    render(){
        return(
            <Switch>
                <Route path={`${this.props.match.path}/articles/:id`} component={Article} />
                <Route path={this.props.match.path} component={Articles}/>
                <Route path={`${this.props.match.path}/articles/`} component={Articles} />
            </Switch>
        )
    }
}

export default Blog;