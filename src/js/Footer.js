import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

class Footer extends Component {

    render(){
        return(
            <footer>
                <div className="wggrid wrap-footer">

                    <div className="row">
                        <div className="bottom-social">
                            <div id='cssmenu' className="align-center">
                                <ul>	
                                    <li><NavLink exact to="/" activeClassName="active"><span>Главная</span></NavLink></li>
                                    <li><NavLink to="/portfolio" activeClassName="active"><span>Портфолио</span></NavLink></li>
                                    <li><NavLink to="/team" activeClassName="active"><span>Команда</span></NavLink></li>
                                    <li><NavLink to="/blog" activeClassName="active"><span>Блог</span></NavLink></li>
                                    <li><NavLink to="/contacts" activeClassName="active"><span>Контакты</span></NavLink></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-1-3">
                            <div className="copyright">
                                &copy; {(new Date()).getFullYear()} Made by – <a href="/">WG team</a>
                            </div>
                        </div>
                        <div className="col-1-3 footer-logo">
                            <Link className="site-branding" to="/">
                                <span>WG</span>
                            </Link>
                        </div>
                        <div className="col-1-3">
                            <ul className="list-inline bottom-social">
                                <a target="_blank" rel="noopener noreferrer" href="https://fb.com"><i className="fa fa-facebook"></i></a>
                                <a target="_blank" rel="noopener noreferrer" href="https://instagram.com"><i className="fa fa-instagram"></i></a>
                                <a target="_blank" rel="noopener noreferrer" href="https://vk.com"><i className="fa fa-vk"></i></a>
                                <a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org"><i className="fa fa-bitbucket"></i></a>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
            </footer>
        )
    }
}

export default Footer;