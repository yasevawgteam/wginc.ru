import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactFancyBox from 'react-fancybox';
import 'react-fancybox/lib/fancybox.css';
import Loader from './Loader';

class Article extends Component {
    constructor(props){
        super(props)
        this.state = {
            loader: true,
            project: []
        }
    }

    componentDidMount(){
        fetch(`https://wginc.ru/projects.php?id=${this.props.match.params.id}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    loader: false,
                    project: data
                })
            })
            .catch(e => {
                console.error(e)
            })
    }

    render(){
        if(this.state.loader) return <Loader />
        
        if(this.state.project == null){
            return(
                <article className="single-post wggrid">
                    <div className="row wrap-post">
                        <div className="entry-header">
                            <h2 className="entry-title">
                                Такого проекта не существует
                            </h2>
                        </div>
                    </div>
                </article>
            )
        }

        return(
            <article className="single-post wggrid">
                <div className="row wrap-post">
                    <div className="entry-header">
                        <span className="time">
                            {this.state.project.date}
                        </span>
                        <h1 className="entry-title">
                            {this.state.project.name}
                        </h1>
                    </div>

                    <div className="entry-content" dangerouslySetInnerHTML={{__html:this.state.project.description}}></div>
                    
                    {
                        this.state.project.gallery.map((img, ind) => {
                            return(
                                <div key={ind} className="post-thumbnail-wrap">
                                    <ReactFancyBox thumbnail={`/images/portfolio/${img[0]}`} image={`/images/portfolio/${img[0]}`} />
                                    <div className="entry-content" >{img[1]}</div>
                                </div>
                            )
                        })
                    }
                    <div className="entry-content" >
                        <Link to="/portfolio">&#8592; &nbsp; портфолио</Link>
                        <a className="redirect" rel="noopener noreferrer" target="_blank" href={this.state.project.link}>{this.state.project.link !== "" ? "Перейти на сайт" : ""}</a>
                    </div>
                </div>
            </article>    
        )
    }
}

export default Article;