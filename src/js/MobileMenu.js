import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
 
class MobileMenu extends Component {
  constructor(props){
    super(props)
    this.state = {
      active: false
    }
  }

  toggleClassActive(){
    this.setState({active: !this.state.active})
  }
 
  render () {
    return (
      <div className={this.state.active ? `cssmenu_mob active_mob_menu`: `cssmenu_mob`}>
        <div className="wrap_menu_icon">
          <svg 
            className={this.state.active ? "ham hamRotate ham1 active" : "ham hamRotate ham1"} 
            viewBox="0 0 100 100" 
            width="80" 
            onClick={this.toggleClassActive.bind(this)} >

            <path
                  className="line top"
                  d="m 30,33 h 40 c 0,0 9.044436,-0.654587 9.044436,-8.508902 0,-7.854315 -8.024349,-11.958003 -14.89975,-10.85914 -6.875401,1.098863 -13.637059,4.171617 -13.637059,16.368042 v 40" />
            <path
                  className="line middle"
                  d="m 30,50 h 40" />
            <path
                  className="line bottom"
                  d="m 30,67 h 40 c 12.796276,0 15.357889,-11.717785 15.357889,-26.851538 0,-15.133752 -4.786586,-27.274118 -16.667516,-27.274118 -11.88093,0 -18.499247,6.994427 -18.435284,17.125656 l 0.252538,40" />
          </svg>        
        </div>

        <ul className="mob_menu_list">	
          <li><NavLink exact to="/" activeClassName="active" onClick={this.toggleClassActive.bind(this)} ><span>Главная</span></NavLink></li>
          <li><NavLink to="/portfolio" activeClassName="active" onClick={this.toggleClassActive.bind(this)} ><span>Портфолио</span></NavLink></li>
          <li><NavLink to="/team" activeClassName="active" onClick={this.toggleClassActive.bind(this)} ><span>Команда</span></NavLink></li>
          <li><NavLink to="/blog" activeClassName="active" onClick={this.toggleClassActive.bind(this)} ><span>Блог</span></NavLink></li>
          <li><NavLink to="/contacts" activeClassName="active" onClick={this.toggleClassActive.bind(this)} ><span>Контакты</span></NavLink></li>
        </ul>      
      </div>

    );
  }
}

export default MobileMenu;