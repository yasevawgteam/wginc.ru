import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import MobileMenu from './MobileMenu';

class Header extends Component {

    render(){
        return(
			<header className="main-header">
				
				<MobileMenu />
			
				<div className="wggrid">
					<div id="top">
						<div className="row">
							<div className="col-3-4">
								<ul className="list-inline top-link link">
									<li><a target="_blank" rel="noopener noreferrer" href="https://fb.com"><i className="fa fa-facebook"></i></a></li>
									<li><a target="_blank" rel="noopener noreferrer" href="https://instagram.com"><i className="fa fa-instagram"></i></a></li>
									<li><a target="_blank" rel="noopener noreferrer" href="https://vk.com"><i className="fa fa-vk"></i></a></li>
									<li><a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org"><i className="fa fa-bitbucket"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div className="t-center">
						<Link className="site-branding" to="/">
							<span id="logo">WG team</span>
						</Link>
					
						<div id='cssmenu' className="align-center">
							<ul>	
								<li><NavLink exact to="/" activeClassName="active"><span>Главная</span></NavLink></li>
								<li><NavLink to="/portfolio" activeClassName="active"><span>Портфолио</span></NavLink></li>
								<li><NavLink to="/team" activeClassName="active"><span>Команда</span></NavLink></li>
								<li><NavLink to="/blog" activeClassName="active"><span>Блог</span></NavLink></li>
								<li><NavLink to="/contacts" activeClassName="active"><span>Контакты</span></NavLink></li>
							</ul>
						</div>
					</div>
				</div>   
			</header>
        );
    }
}

export default Header;