import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './Home';
import Portfolio from './Portfolio';
import Team from './Team';
import Blog from './Blog';
import Contacts from './Contacts';

class Body extends Component {
    render(){
        return(
            <section id="container">
                <div className="wrap-container">
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/portfolio" component={Portfolio} />
                        <Route path="/team" component={Team} />
                        <Route path="/blog" component={Blog} />
                        <Route path="/contacts" component={Contacts} />                    
                    </Switch>
                </div>
            </section>
        )
    }
}
export default Body;