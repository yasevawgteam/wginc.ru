import React, { Component } from 'react';

class Team extends Component {

    render(){
        return(
            <div id="main-content">
                <div className="wrap-content">
                    <div className="wggrid">
                        {/* <div className="row wrap-team">

                            <div className="entry-header">
                                <h1 className="entry-title">Коробчук Виталий</h1>
                            </div>

                            <div className="col-1-3">
                                <div className="wrap-col">
                                    <a target="_blank" className="portfolio-box zoom-effect" href="https://instagram.com/vitalykorobchuk/">
                                    <img src="/images/team/kratos.jpg" className="img-responsive" alt="" />
                                    <div className="portfolio-box-caption">
                                        <div className="portfolio-box-caption-content">
                                        <div className="project-name">
                                            <span>@vitalykorobchuk</span>
                                        </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            </div>

                            <div className="col-2-3">
                                <div className="entry-content">
                                    <p>Location: Padua, Italy</p>
                                    <p></p>
                                    <p>Email: vitalykorobchuk@gmail.com</p>
                                    <p></p>
                                    <p>Tel: +7 (777) 777 - 77 - 77</p>
                                    <p></p>
                                    <p><i className="fa fa-skype"></i> vitalykorobchuk@gmail.com</p>
                                </div>
                                <div className="entry-content">    
                                    <a href="#"><i className="fa fa-facebook"></i></a>
                                    <a href="#"><i className="fa fa-instagram"></i></a>
                                    <a href="#"><i className="fa fa-vk"></i></a>
                                    <a href="#"><i className="fa fa-bitbucket"></i></a>
                                    <a href="#"><i className="fa fa-github"></i></a>
                                </div>
                            </div>

                        </div> */}
                        
                        <div className="row wrap-team">

                            <div className="entry-header">
                                <h1 className="entry-title">Ясевич Дмитрий</h1>
                            </div>

                            <div className="col-1-3">
                                <div className="wrap-col">
                                    <a target="_blank" rel="noopener noreferrer" className="portfolio-box zoom-effect" href="https://instagram.com/wgyasick/">
                                    <img src="/images/team/yasick.jpg" className="img-responsive" alt="" />
                                    <div className="portfolio-box-caption">
                                        <div className="portfolio-box-caption-content">
                                        <div className="project-name">
                                            <span>@wgyasick</span>
                                        </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            </div>

                            <div className="col-2-3">
                                <div className="entry-content">
                                    <p>Москва, Россия</p>
                                    <p></p>
                                    <p><i className="fa fa-envelope"></i> <a href="mailto:yasevawgteam@gmail.com">yasevawgteam@gmail.com</a></p>
                                    <p></p>
                                    <p><i className="fa fa-skype"></i> <a href="skype:yasevawgteam@gmail.com">yasevawgteam@gmail.com</a></p>
                                </div>
                                <div className="entry-content">    
                                    <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/yaseva.demon"><i className="fa fa-facebook"></i></a>
                                    <a target="_blank" rel="noopener noreferrer" href="https://instagram.com/wgyasick/"><i className="fa fa-instagram"></i></a>
                                    <a target="_blank" rel="noopener noreferrer" href="https://vk.com/yaseva_demon"><i className="fa fa-vk"></i></a>
                                    <a target="_blank" rel="noopener noreferrer" href="https://bitbucket.org/yasevawgteam/"><i className="fa fa-bitbucket"></i></a>
                                    {/* <a target="_blank"  href="#"><i className="fa fa-github"></i></a> */}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Team;